//  Copyright (c) 2016 - Alain Fourmigue - All rights reserved.

#include<iostream>
#include <tuple>
#include<algorithm>
#include<vector>
#include <cassert>
#include "heatmap.h"
#include "ppm.hpp"

using namespace std;

class Palette 
{
public:
    Palette(double,double);
    double GetRed(double) const;
    double GetGreen(double) const;
    double GetBlue(double) const;
private:
    double GetRatio(double) const;
    double minValue{};
    double maxValue{};
};

class Axis
{
public:
    int GetMinIndex(double) const;
    int GetMaxIndex(double) const;
    int GetNumberPixels() const;
    double GetSize() const;
    void SetCoord(double);
    void Finalize(int);    

private:    
    double minCoord{};
    double maxCoord{};
    int numberPixels{};
    vector<double> coords;
    vector<bool> marked;
    int GetPixelIndex(double) const;
    bool finalized{};
};

struct heatmap
{
    void Set(Real,Real,Real);
    void Write(const string&,int,double,double);
    Axis axisX;
    Axis axisY;
    using Cell = tuple<Real,Real,Real>;
    vector<Cell> cells;
};

Heatmap* create_heatmap()
{
    return new Heatmap;
}

void set_heatmap(Heatmap* heatmap,Real coordX,Real coordY,Real temperature)
{
    heatmap->Set(coordX,coordY,temperature);
}

void write_heatmap(
    Heatmap* heatmap,
    const char* path,
    int numberPixels,
    Real minTemp,
    Real maxTemp
){
    try
    {
        heatmap->Write(string(path),numberPixels,minTemp,maxTemp);
    }
    catch(const exception& error)
    {
        cout << "Error: " << error.what() << endl;
    }
}

void free_heatmap(Heatmap* heatmap)
{
    delete heatmap;
}

void Heatmap::Set(Real coordX,Real coordY,Real temperature)
{
    axisX.SetCoord(coordX);
    axisY.SetCoord(coordY);
    cells.emplace_back(coordX,coordY,temperature);
}

void Heatmap::Write(
    const string& path,
    int numberPixels,
    double minTemperature,
    double maxTemperature
){
    if( cells.size() < 4 ){ throw runtime_error("too few values"); }

    axisX.Finalize(numberPixels);
    numberPixels = static_cast<int>(axisY.GetSize() * numberPixels / axisX.GetSize());    
    axisY.Finalize(numberPixels);

    Palette palette(minTemperature,
                    maxTemperature);

    Ppm ppm(axisX.GetNumberPixels(),
            axisY.GetNumberPixels());

    for(const auto& cell : cells)
    {
        double coordX,coordY,temperature;
        tie(coordX,coordY,temperature) = cell;

        ppm.SetFillColor(
            palette.GetRed(temperature),
            palette.GetGreen(temperature),
            palette.GetBlue(temperature));

       
        ppm.DrawRectangle(
            axisX.GetMinIndex(coordX),
            axisY.GetMinIndex(coordY),
            axisX.GetMaxIndex(coordX),
            axisY.GetMaxIndex(coordY));
    }
    ppm.Write(path);
}

void Axis::SetCoord(double coord)
{
    minCoord = coords.empty() ? coord : min(coord,minCoord);
    maxCoord = coords.empty() ? coord : max(coord,maxCoord);

    coords.push_back(coord);
}

void Axis::Finalize(int numPixels)
{
    if( numPixels < 2 ){ throw runtime_error("too few pixels"); }

    if( minCoord >= maxCoord ){ throw runtime_error("zero-length heatmap"); }

    numberPixels = numPixels;
    marked.assign(numberPixels,false);

    for(const auto coord : coords)
    {
        marked[GetPixelIndex(coord)] = true;
    }
    finalized = true;
}

int Axis::GetPixelIndex(double coord) const
{       
    auto ratio = (coord - minCoord) / (maxCoord - minCoord);      
    
    auto index = static_cast<int>(ratio * (numberPixels-1));

    return index;
}

int Axis::GetMinIndex(double coord) const
{
    int index = GetPixelIndex(coord);
    
    int n = 1;
    while( index >= n && !marked[index-n]){ ++n; }   

    return index - n/2;
}

int Axis::GetMaxIndex(double coord) const
{
    int index = GetPixelIndex(coord);
    
    int n = 1;
    while( index + n < numberPixels && !marked[index+n]){ ++n; }   

    return index + n/2;
}

double Axis::GetSize() const
{
    return maxCoord - minCoord;
}

int Axis::GetNumberPixels() const
{
    return numberPixels;
}

Palette::Palette(double minValue,double maxValue):
minValue    ( minValue ),
maxValue    ( maxValue )
{
    if( minValue >= maxValue )
    { 
        throw runtime_error("null temperature gradient"); 
    }
}

double Palette::GetRatio(double value) const
{
    value = max(value,minValue);
    value = min(value,maxValue);

    return (value - minValue) / (maxValue - minValue);
}

double Palette::GetRed(double value) const
{
    auto ratio = GetRatio(value);

    if( ratio <= 0.5 )
    { 
        return 0.0; 
    }
    else if( ratio <= 0.75 )
    { 
        return 4 * (ratio - 0.5); 
    }
    return 1.0;
}

double Palette::GetGreen(double value) const
{
    auto ratio = GetRatio(value);

    if( ratio <= 0.25 )
    { 
        return 4 * ratio;
    }
    else if( ratio <= 0.75 )
    { 
        return 1.0;
    }
    return 4 * (1 - ratio);
}

double Palette::GetBlue(double value) const
{
    auto ratio = GetRatio(value);

    if( ratio <= 0.25 )
    { 
        return 1.0; 
    }
    else if( ratio <= 0.5 )
    { 
        return 4 * (0.5 - ratio);  
    }
    return 0.0;
}


