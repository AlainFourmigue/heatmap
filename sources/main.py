# Copyright (c) 2016 - Alain Fourmigue - All Rights Reserved 

import sys
import csv
import math
import os
import ctypes 
import argparse 
import Image # This module is used for the conversion ppm to png

class Error(Exception):
    """ Base-class for all exceptions in this module """

class FileError(Error):
    """ Base-class for all exceptions thrown while reading input file """
    def __init__(self,path):
        self.path = path

class OpenError(FileError):
    """ Failed to open file """
    def __init__(self,path):
        self.path = path

class FormatError(FileError): 
    """ Found an invalid number of values """
    def __init__(self,path,line,count):
        self.path = path
        self.line = line
        self.count = count

class FloatError(FileError): 
    """ Failed to convert string to float """
    def __init__(self,path,line,what):
        self.path = path
        self.line = line
        self.what = what

def eq(a,b):
    """ This function tests the equality of two floats """
    return math.fabs(a-b) < 1e-10

def read(path):
    """ This generator reads a csv file and yiels 4 values at each line """
    try:
        with open(path) as fd:
            reader = csv.reader(fd,delimiter=' ')
            for line,row in enumerate(reader,1):
                if row and not row[0].startswith('%'):
                    items = [item for item in row if item]
                    if len(items) != 4:
                        raise FormatError(path,line,len(items))
                    try:
                        yield tuple(float(item) for item in items)
                    except ValueError as error:
                        raise FloatError(path,line,error.message)
    except IOError:
        raise OpenError(path)

def ppmtopng(path):
    """ This function converts a ppm image to png """
    if os.path.isfile(path):
        im = Image.open(path)
        base, ext = os.path.splitext(path)
        im.save(base + '.png')
        os.remove(path)

class Heatmap:
    numpixel = 1000
    def __init__(self,zcoord):
        self.zcoord = zcoord
        self.data = list()
        self.minvalue = float('+inf') 
        self.maxvalue = float('-inf')

    def set(self,x,y,v):
        self.data.append((x,y,v))
        self.minvalue = min(v,self.minvalue)
        self.maxvalue = max(v,self.maxvalue)

    def write(self,lib,path,minvalue,maxvalue):
        if minvalue >= maxvalue:
            print "Error: can't write  heatmap at z-coord",self.zcoord
            print ' -> min-value (',minvalue,') is greater than max-value (',maxvalue,')'
            return
        handle = lib.create_heatmap()
        for x,y,v in self.data:
            lib.set_heatmap(
                handle,
                ctypes.c_double(x),
                ctypes.c_double(y),
                ctypes.c_double(v))
        base, ext = os.path.splitext(path)
        path = base + '_' + str(int(self.zcoord*1e6))+'um.ppm'
        print 'Wrinting ',path
        print ' -> color scale ranges from',minvalue,'C to',maxvalue,'C'
        lib.write_heatmap(
            handle,
            path,
            ctypes.c_int(self.numpixel),
            ctypes.c_double(minvalue),
            ctypes.c_double(maxvalue))
        lib.free_heatmap(handle)
        ppmtopng(path)

    def stats(self):
        print 'z-coord =', self.zcoord,'  ',
        print 'min-temperature =', self.minvalue,'  ',
        print 'max-temperature =', self.maxvalue

def parse():
    parser = argparse.ArgumentParser(
        description='This program generates heat maps')
    parser.add_argument('path',
        help='path to the file that contains the data')
    parser.add_argument('-s','--stats',action='store_true',
        help='show statistics')
    parser.add_argument('--min',action='store',type=float,metavar='<value>',
        help='start color scale at temperature <value>')
    parser.add_argument('--max',action='store',type=float,metavar='<value>',
        help='end color scale at temperature <value>')
    args = parser.parse_args()
    return args

try:
    args = parse()

    data = read(args.path)

    zcoord = float('-inf')
    heatmaps = list()
    for x,y,z,v in data:
        if not eq(z,zcoord):
            zcoord = z
            heatmaps.append(Heatmap(zcoord))
        heatmaps[-1].set(x,y,v)

    if args.stats:
        for index,heatmap in enumerate(heatmaps,1):
            print 'Heatmap',index,': ',
            heatmap.stats()
    else:
        lib = ctypes.cdll.LoadLibrary('libheatmap.so')
        for heatmap in heatmaps:
            minvalue = args.min if args.min else heatmap.minvalue
            maxvalue = args.max if args.max else heatmap.maxvalue
            heatmap.write(lib,args.path,minvalue,maxvalue)
   
except OpenError as error:
    print 'Error:'
    print '> file:',error.path
    print '> what: file not found'

except FormatError as error:
    print 'Error:'
    print '> file:',error.path
    print '> line:',error.line
    print '> what: found',error.count,'values, expected 4 values'

except FloatError as error:
    print 'Error:'
    print '> file:',error.path
    print '> line:',error.line
    print '> what:',error.message

except Exception as error:
    print 'Error:' 
    print '> what:',error.message

finally:
    sys.exit()
 
