//  Copyright (c) 2016 - Alain Fourmigue - All rights reserved.

#include<fstream>
#include <string>
#include<stdexcept>
#include "ppm.hpp"

using namespace std;

const string Ppm::magicNumber{"P6"};

Ppm::Ppm(int numberPixelX,int numberPixelY):
axisX   (numberPixelX),    
axisY   (numberPixelY)
{
    pixels.resize(axisX.GetNumberPixels() *
                  axisY.GetNumberPixels());
}

void Ppm::SetFillColor(double red,double green,double blue)
{
    filler.red   = static_cast<char>(red   * maxColorComponent);
    filler.green = static_cast<char>(green * maxColorComponent);
    filler.blue  = static_cast<char>(blue  * maxColorComponent);
}

void Ppm::DrawRectangle(int imin,int jmin,int imax,int jmax)
{
    for(int j = jmin; j <= jmax; ++j)
    {
        for(int i = imin; i <= imax; ++i)
        {
            GetPixel(i,j) = filler;
        }
    }
}

Ppm::Pixel& Ppm::GetPixel(int i,int j)
{
    if(!axisX.Contains(i) || !axisY.Contains(j)){ throw PixelOutOfImage(); }

    j = axisY.GetNumberPixels() - 1 - j;
    
    return pixels[j * axisX.GetNumberPixels() + i];
}

void Ppm::Write(const string& path) const
{
    ofstream file(path);
    file << string(magicNumber) << " ";
    file << axisX.GetNumberPixels() << " "; 
    file << axisY.GetNumberPixels() << " ";
    file << maxColorComponent << endl;

    const char* buffer = reinterpret_cast<const char*>(&pixels[0]);
    file.write(buffer,pixels.size()*sizeof(Pixel));
}

Ppm::Axis::Axis(int numberPixels):
numberPixels (numberPixels)
{
    if( numberPixels < minNumberPixels ){ throw TooFewPixels(); }

    if( numberPixels > maxNumberPixels ){ throw TooManyPixels(); }
}

int Ppm::Axis::GetNumberPixels() const
{
    return numberPixels;
}

bool Ppm::Axis::Contains(int index) const
{
    return (index >= 0) && (index < numberPixels);
}


