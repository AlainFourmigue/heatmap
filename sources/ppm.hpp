//  Copyright (c) 2016 - Alain Fourmigue - All rights reserved.

#include <vector>

class Ppm
{
public:
    Ppm(int,int);
    void SetFillColor(double,double,double);
    void DrawRectangle(int,int,int,int);
    void Write(const std::string&) const;

private:
    struct Pixel { char red, green, blue; };
    std::vector<Pixel> pixels;
    class PixelOutOfImage: public std::exception {};
    Pixel& GetPixel(int,int);
    Pixel filler;

    class Axis
    {
    public:
        Axis(int);
        bool Contains(int) const;   
        int GetNumberPixels() const; 
        class TooManyPixels: public std::exception {};        
        class TooFewPixels: public std::exception {};
    private:
        int numberPixels;
        static constexpr int maxNumberPixels{3000};        
        static constexpr int minNumberPixels{2};        
    };    
    Axis axisX, axisY;
    
    static const std::string magicNumber;
    static constexpr int maxColorComponent{255};
};
