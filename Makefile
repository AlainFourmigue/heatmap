include makefile.def

# Compile the library
all: 
	( cd sources && $(MAKE) )

# Remove all the files not in the original distribution
clean: 
	$(RM) build
	$(RM) examples/*png
	( cd sources  && $(MAKE) clean )

# Compile the library (if needed) and install it
install: all 
	cp build/lib$(PROJECT).so /usr/lib/




