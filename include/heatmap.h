//  Copyright (c) 2016 - Alain Fourmigue - All rights reserved.

#pragma once

typedef double Real;
typedef struct heatmap Heatmap;

// This macro tells Microsoft Visual Studio Compiler to export symbols
#if _MSC_VER
#define symbol __declspec(dllexport)     
#else
#define symbol 
#endif

extern "C"
{

// This function creates an empty heat map
symbol Heatmap* create_heatmap();

// This function sets the temperature at point x,y
symbol void set_heatmap
(
    Heatmap*    heatmap,    
    Real        coordX,
    Real        coordY,
    Real        temperature
);

// This function writes the heat map to a ppm picture file
symbol void write_heatmap
(
    Heatmap*    heatmap,
    const char* path,
    int         numberPixels,
    Real        minTemperature,
    Real        maxTemperature
);

// This function frees the memory used by the heat map 
symbol void free_heatmap(Heatmap*);

}
