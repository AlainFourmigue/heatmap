# Welcome to Heatmap!

## Introduction

Heatmap is a tool for creating temperature maps.  Heatmap is written in c++ 
and interfaced through a python script that reads the data from csv files.

## Getting started

1. Compile heatmap at the command prompt:

        $ cd heatmap
        $ make
        $ sudo make install

   The library libheatmap.so should now be installed in the directory /usr/lib.
   The python script 'heatmap.py' that reads the data from csv files is located 
   in the directory heatmap/build/.

2. Run the example at the command prompt:

        $ cd examples/
        $ python ../build/heatmap.py data.csv 

## License

Heatmap is released under the [GNU Lesser General Public License v3](http://www.gnu.org/licenses)

## Contacts

If you have any question or if you would like to request additional features,
please contact the author at alain.fourmigue@polymtl.ca

